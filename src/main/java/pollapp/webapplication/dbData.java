/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pollapp.webapplication;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import pollapp.webapplication.model.Question;
import pollapp.webapplication.rest.RestClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 * @author meniol
 */
@ManagedBean
public class dbData {

    private String t1;
    private String t2;
    private String t3;

    private String przedmioty;
    private Map<String, String> przedmiot = new HashMap<String, String>();
    
    private String city;  
    private Map<String,String> cities = new HashMap<String, String>();
    

    private List<Question> pytania = new ArrayList<>();

    public List<Question> getPytania() {
        return pytania;
    }

    public void setPytania(List<Question> pytania) {
        this.pytania = pytania;
    }

    
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Map<String, String> getCities() {
        return cities;
    }

    public void setCities(Map<String, String> cities) {
        this.cities = cities;
    }
    @PostConstruct
    public void init() {
        
        cities = new HashMap<String, String>();
        cities.put("New York", "New York");
        cities.put("London","London");
        cities.put("Paris","Paris");
        cities.put("Barcelona","Barcelona");
        cities.put("Istanbul","Istanbul");
        cities.put("Berlin","Berlin");
        
        
        
        przedmiot = new HashMap<String, String>();

        przedmiot.put("ktos1", "przedmiot1");
        przedmiot.put("ktos2", "przedmiot2");
        przedmiot.put("ktos3", "przedmiot3");
        przedmiot.put("ktos4", "przedmiot4");
        przedmiot.put("ktos5", "przedmiot5");
        
        RestClient client = new RestClient();
        try {
            Response<List<Question>> response = client.get().questions().execute();
            if(response.code() == 200){
                pytania.addAll(response.body());
            }else{
                
            }
            

        } catch (IOException ex) {
            Logger.getLogger(dbData.class.getName()).log(Level.SEVERE, null, ex);
        }
        
      
    }

    public HashMap<String, String> przedList() {
        HashMap<String, String> przedmiot = new HashMap<String, String>();
                
        przedmiot.put("ktos1", "przedmiot1");
        przedmiot.put("ktos2", "przedmiot2");
        przedmiot.put("ktos3", "przedmiot3");
        przedmiot.put("ktos4", "przedmiot4");
        przedmiot.put("ktos5", "przedmiot5");
        
        return przedmiot;

    }

    public List<String> compList() {
        List<String> results = new ArrayList<String>();
        for (int i = 0; i < 10; i++) {
            results.add("" + i);
        }

        return results;
    }

    public String getT1() {
        return t1;
    }

    public void setT1(String t1) {
        this.t1 = t1;
    }

    public String getT2() {
        return t2;
    }

    public void setT2(String t2) {
        this.t2 = t2;
    }

    public String getT3() {
        return t3;
    }

    public void setT3(String t3) {
        this.t3 = t3;
    }

    public String getPrzedmioty() {
        return przedmioty;
    }

    public void setPrzedmioty(String przedmioty) {
        this.przedmioty = przedmioty;
    }

    public Map<String, String> getPrzedmiot() {
        return przedmiot;
    }

    public void setPrzedmiot(Map<String, String> przedmiot) {
        this.przedmiot = przedmiot;
    }

}
