/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pollapp.webapplication;

/**
 *
 * @author meniol
 */
import java.io.IOException;
import java.util.logging.Level;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pollapp.webapplication.model.Student;
import pollapp.webapplication.rest.RestClient;
import retrofit2.Call;
import retrofit2.Response;


@ManagedBean
@SessionScoped
public class UserManager {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(UserManager.class);
    
    public static final String HOME_PAGE_REDIRECT = "/secure/mainpage.xhtml?faces-redirect=true";
    public static final String ADMIN_PAGE_REDIRECT = "/secure/adminpage.xhtml?faces-redirect=true";
    public static final String LOGOUT_PAGE_REDIRECT = "/logout.xhtml?faces-redirect=true";
    public static final String LOGIN_PAGE_REDIRECT = "/login.xhtml?faces-redirect=true";

    private String userLogin;
    private String userPassword;
    private Student currentUser;


    public String logout() {
        String identifier = userLogin;

//        // invalidate the session
//        LOGGER.debug("invalidating session for '{}'", identifier);
        FacesContext.getCurrentInstance().getExternalContext()
                .invalidateSession();
//
//        LOGGER.info("logout successful for '{}'", identifier);
        return LOGOUT_PAGE_REDIRECT;
    }

    public String loginpage() {
        return LOGIN_PAGE_REDIRECT;
    }

    public boolean isLoggedIn() {
        return currentUser != null;
    }

    public String isLoggedInForwardHome() {
        if (isLoggedIn()) {
            return HOME_PAGE_REDIRECT;
        }

        return null;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public Student getCurrentUser() {
        return currentUser;
    }

    public Response<Student> restLogin(final String username, final String password) throws IOException {

        RestClient client = new RestClient();
        Call<Student> cell = client.get().login(userLogin, userPassword);

        return cell.execute();
    }

    public String checkValidUser() {

        Response<Student> response;
        try {
            response = restLogin(userLogin, userPassword);
            
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Błąd połączenia z serwerem"
                            + "Hasło",
                            "Invalid or unknown credentials."));

            return null;
        }

   
        System.out.println(userLogin);

        if (response.code() == 200) {
            currentUser = response.body();
            
            if (userLogin.equals("admin") && userPassword.equals("admin")) {
                System.out.println("LOGOWANIE NA ADMINA POWIODŁO SIE");
                return ADMIN_PAGE_REDIRECT;
            } else {
                System.out.println("LOGOWANIE POWIODŁO SIE");
                return HOME_PAGE_REDIRECT;
            }

        } else {
//            LOGGER.info("login failed for '{}'", userLogin);
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Błąd logowania: "
                            + "Login",
                            "Invalid or unknown credentials."));

            return null;
        }
    }

}
