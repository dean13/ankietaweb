package pollapp.webapplication.rest;



import java.util.List;

import okhttp3.ResponseBody;
import pollapp.webapplication.model.Groups;
import pollapp.webapplication.model.Lecturer;
import pollapp.webapplication.model.Question;
import pollapp.webapplication.model.Student;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Pantera on 2016-03-23.
 */
public interface ApiRest {
    @Headers("Content-Type: application/json")
    @POST("login")
    Call<Student> login(@Query("username") String username, @Query("password") String password);

    @POST("/logout")
    Call<ResponseBody> logout();



    @GET("v1/lecturer/all/{g}")
    Call<List<Lecturer>> lecturer(@Path("g") String group);

    @GET("v1/question/all")
    Call<List<Question>> questions();

    @POST("v1/questionnaire/insert")
    Call<ResponseBody> insertQuestionnaire(@Query(value = "lecturer", encoded = false) String lecturer,
                                           @Query("studentname") String studentName,
                                           @Query("totalpoints") float totalPoints);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @GET("v1/group/getGroupByStudentId/{id}")
    Call<Groups> getGroupStudents(@Path("id") int id);


}

