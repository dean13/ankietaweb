/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pollapp.webapplication;

/**
 *
 * @author meniol
 */
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;


@ManagedBean
@SessionScoped
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    private String userId;
    private String login;
    private String pass;

    public User(String userId, String login, String pass) {
        super();

        this.userId = userId;
        this.login = login;
        this.pass = pass;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return login;
    }

    public void setFirstName(String login) {
        this.login = login;
    }

    public String getLastName() {
        return pass;
    }

    public void setLastName(String pass) {
        this.pass = pass;
    }

    public String getName() {
        return login + " " + pass;
    }

    public String toString() {
        return "user[userId=" + userId + ", firstName=" + login
                + ", lastName=" + pass + "]";
    }
    
    
  
}