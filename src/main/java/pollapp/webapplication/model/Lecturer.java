package pollapp.webapplication.model;

/**
 * Created by Krzysiek on 18.05.2016.
 */
public class Lecturer {
    private int idLecturer;
    private String name;
    private String lastname;
    private String title;
    private float totalpoint;

    public int getIdLecturer() {
        return idLecturer;
    }

    public void setIdLecturer(int idLecturer) {
        this.idLecturer = idLecturer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getTotalpoint() {
        return totalpoint;
    }

    public void setTotalpoint(float totalpoint) {
        this.totalpoint = totalpoint;
    }
}
