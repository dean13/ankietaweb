package pollapp.webapplication.model;

/**
 * Created by Pantera on 2016-05-23.
 */
public class Groups {

    private int idGroupe;
    private String name;

    public int getIdGroupe() {
        return idGroupe;
    }

    public void setIdGroupe(int idGroupe) {
        this.idGroupe = idGroupe;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
