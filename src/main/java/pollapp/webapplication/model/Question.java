package pollapp.webapplication.model;

/**
 * Created by Krzysiek on 19.05.2016.
 */
public class Question {
    private String idQuestion;
    private String question;
    private float rating;
    

    public String getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(String idQuestion) {
        this.idQuestion = idQuestion;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}


