/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pollapp.webapplication;

/**
 *
 * @author Meniol
 */
import javax.annotation.PostConstruct;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.PieChartModel;
 
@ManagedBean
public class ChartView implements Serializable {
 
    private PieChartModel pieModel2;
    private BarChartModel barModel;


 
    @PostConstruct
    public void init() {
        createPieModels();
        createBarModels();
    }
 
    public BarChartModel getBarModel() {
        return barModel;
    }
    
    public PieChartModel getPieModel2() {
        return pieModel2;
    }
     
    private void createPieModels() {
        createPieModel2();
    }
 
    private BarChartModel initBarModel() {
        BarChartModel model = new BarChartModel();
 
        ChartSeries boys = new ChartSeries();
        boys.setLabel("Ocena");
        boys.set("ktos1", 2.4);
        boys.set("ktos2", 5);
        boys.set("ktos3", 3);
        boys.set("ktos4", 2);
        boys.set("ktos5", 1);
 
        
 
        model.addSeries(boys);
        
         
        return model;
    }
     
    private void createBarModels() {
        createBarModel();
    }
     
    private void createBarModel() {
        barModel = initBarModel();
         
        barModel.setTitle("Statystyka ogólna");
        barModel.setLegendPosition("ne");
         
        Axis xAxis = barModel.getAxis(AxisType.X);
        xAxis.setLabel("Prowadzący");
         
        Axis yAxis = barModel.getAxis(AxisType.Y);
        yAxis.setLabel("Ocena");
        yAxis.setMin(0);
        yAxis.setMax(5);
    }
     
    private void createPieModel2() {
        pieModel2 = new PieChartModel();
         
        pieModel2.set("5", 540);
        pieModel2.set("4", 325);
        pieModel2.set("3", 702);
        pieModel2.set("2", 421);
        pieModel2.set("1", 540);
        
         
        pieModel2.setTitle("Oceny prowadzącego");
        pieModel2.setLegendPosition("e");
        pieModel2.setFill(false);
        pieModel2.setShowDataLabels(true);
        pieModel2.setDiameter(150);
    }
    
    
 
   
     
}
